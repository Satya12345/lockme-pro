package com.simplilearn.lockme.model;

public class UserCredentials {
	
	private String siteName;

	private String loggedUSer;
	private String username;
	private String password;
	public UserCredentials() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserCredentials(String siteName, String loggedUSer, String username, String password) {
		super();
		this.siteName = siteName;
		this.loggedUSer = loggedUSer;
		this.username = username;
		this.password = password;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getLoggedUSer() {
		return loggedUSer;
	}
	public void setLoggedUSer(String loggedUSer) {
		this.loggedUSer = loggedUSer;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "UserCredentials [siteName=" + siteName + ", loggedUSer=" + loggedUSer + ", username=" + username
				+ ", password=" + password + "]";
	}
	
	

}
